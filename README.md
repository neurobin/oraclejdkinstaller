OracleJDKInstaller
==================

Oracle JDK installler script for linux

Installation Instruction:
=========================

    1. Put the oraji file inside the folder where you have downloaded the archive of oracle jdk (tar.gz)
    2. give the install script execution permission (sudo chmod +x path_to_the_folder/oraji)
    3. now run the script oraji in terminal, or just drag and drop it in terminal and hit enter.
    4. It will ask for version number. Give the version number. (Example: 8 or 8u25, must match with the archive)


Tested OS:
----------
    1. Xubuntu 14.04 LTS
